import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {IPerson} from '../interfaces/people.interface';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class PeopleService {
  private apiUrl = `${environment.swapiUrl}/people`;

  constructor(private http: HttpClient) {}

  async getPeople(name?: string, url?: string, people?: IPerson[], ...args): Promise<IPerson[]> {
    url = !!url ? url : !!name ? `${this.apiUrl}?search=${name}`: this.apiUrl;
    people = people || [];

    // Since we want to apply pagination with 6 elements per page and the api does return 10.
    // It's not the fastest solution. We'll use recursion to get all the people and then make our own pagination
    return new Promise<IPerson[]>(async (resolve, reject) => {
      try {
        const response = await this.http.get<any>(url).toPromise();
        people = people.concat(response.results);

        if(!!response.next) {
          resolve(await this.getPeople(null, response.next, people, resolve, reject));
        } else {
          resolve(people);
        }
      } catch(error) {
        console.log(error)
        reject(error)
      }
    });
  }

  getPerson(id: number): Observable<IPerson> {
    return this.http.get<IPerson>(`${this.apiUrl}/${id}`)
  }
}
