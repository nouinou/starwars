import { Component, OnInit } from '@angular/core';
import {PeopleService} from '../../../shared/services/people.service';
import {IPerson} from '../../../shared/interfaces/people.interface';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  person: IPerson;
  isFetching = true;

  constructor(private peopleService: PeopleService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isFetching = true;
    const personId = Number(this.route.snapshot.paramMap.get('id'));
    this.peopleService.getPerson(personId).subscribe((person: IPerson) => {
      this.person = person;
      this.isFetching = false;
      console.log(person)
    })
  }

}
