import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleComponent } from './people.component';
import {PeopleRoutingModule} from './people-routing.module';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import { PersonComponent } from './person/person.component';

@NgModule({
  declarations: [PeopleComponent, PersonComponent],
  imports: [
    CommonModule,
    PeopleRoutingModule,
    FormsModule,
    NgbPaginationModule
  ]
})
export class PeopleModule { }
