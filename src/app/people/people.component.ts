import {Component, OnInit} from '@angular/core';
import {PeopleService} from '../../shared/services/people.service';
import {IPerson} from '../../shared/interfaces/people.interface';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {
  people: IPerson[];
  shownPeople: IPerson[];
  page = 1;
  pageSize = 6;
  collectionSize;
  isFetching ;
  searchName = '';

  constructor(private peopleService: PeopleService, private router: Router, private route: ActivatedRoute) {}

  async ngOnInit(): Promise<void> {
    await this.getPeople();
  }

  refreshPeople(): void {
    this.shownPeople = this.people
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  async getPeople(name?: string): Promise<void> {
    this.isFetching = true;
    this.people = await this.peopleService.getPeople(name);
    this.collectionSize = this.people.length;
    this.refreshPeople();
    this.isFetching = false
  }

  navigateToPerson(personUrl: string): void {
    const personId = personUrl.slice(-2).split('/')[0];
    this.router.navigate([personId], { relativeTo: this.route})
  }

}
