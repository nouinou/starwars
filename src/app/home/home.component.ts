import { Component } from '@angular/core';
import {Router} from '@angular/router';

const DURATION = 1000;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  quote = 'May the force be with you'
  showQuote = false;

  constructor(private router: Router) {}

  onShowQuote() {
    this.showQuote = true;
    setTimeout(() => this.router.navigate(['..','people']), DURATION)
  }
}
