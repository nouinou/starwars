# SqliStarwars

### A Frontend application for Star Wars Characters build with Angular by [Hamza Nouinou](mailto:nouinouhamza@gmail.com)

## Description

This application was done as a 5 Hours challenge using Angular

## How to Install and Run the Project
- you can find the last version of this project here: [https://gitlab.com/nouinou/starwars](https://gitlab.com/nouinou/starwars)
- run `npm install`
- run `npm start` or `ng serve` to serve the project to `localhost:4200`

## APIs and libraries used:
* [Angular 9](https://v9.angular.io/)
* [SCSS](https://sass-lang.com/)
* [RxJS](https://v6.rxjs.dev/)
* [Star Wars API](https://swapi.dev/)
* [NG Bootstrap](https://ng-bootstrap.github.io/)

## screenshots

### Home Page
![HomePage](./screenshots/home.png)

### People Page
![People page](./screenshots/people.png)

### Search Results
![Search Results](./screenshots/search-results.png)

### Person Page
![Person page](./screenshots/person.png)
